from django.utils import translation
from morumbi.cms.models import Page

def navigation(request):
    lang = translation.get_language()
    root_pages = Page.objects.filter(language=lang[:2]).filter(parent__isnull=True).filter(public=True, in_menu=True).select_related('descendants', 'descendants__child').order_by('ordering')
    return {'navigation': root_pages}