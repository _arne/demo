from django import forms
from django.utils.translation import ugettext_lazy as _
from morumbi.cms import template_registry, module_registry

    
class MainSection(object):
    name = 'main'
    verbose_name = _('Main')

class SidebarSection(object):
    name = 'sidebar'
    verbose_name = _('Sidebar')
        
class FooterSection(object):
    name = 'footer'
    verbose_name = _('Footer')


class PageOptionsForm(forms.Form):
    page_title = forms.CharField(label=_(u"title"), max_length=255, required=False)
    meta_description = forms.CharField(label=_(u"description"), max_length=255, required=False)
    meta_keywords = forms.CharField(label=_(u"keywords"), max_length=255, required=False)
    meta_noindex = forms.BooleanField(label=_(u"noindex"), required=False, initial=False)

class DefaultTemplate(object):
    """ A page with sidebar """
    verbose_name = _('Default page')
    template_name = 'pages/default.html'
    sections = [MainSection, SidebarSection, FooterSection]
    options_form = PageOptionsForm


class SimpleTemplate(object):
    """ A page without sidebar """
    verbose_name = _('Simple page')
    template_name = "pages/simple.html"
    sections = [MainSection]
    options_form = PageOptionsForm
            
template_registry.register_template(DefaultTemplate)
template_registry.register_template(SimpleTemplate)

import events.cms

from morumbi.cms.models import BaseModule
from morumbi.cms.forms import ModuleForm

#
# A textile markup text module
#

class TextileForm(ModuleForm):
    text = forms.CharField(widget=forms.Textarea)
        
          
class TextileModule(BaseModule):
    form = TextileForm
    template = "modules/text.html"
    #template = Template("""{% load markup %}{{ text|textile }}""")
    verbose_name = _(u"Text (textile)")
    
    class Meta:
        proxy = True

module_registry.register('textilemodule', TextileModule)


