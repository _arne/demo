from django.contrib import admin
from events.models import Event

admin.site.register(Event, list_display=('title', 'date'), 
                            date_hierarchy='date',
                            search_fields=('title', 'description'))