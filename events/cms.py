from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from morumbi.cms import template_registry, module_registry
from demo.cms import MainSection, PageOptionsForm
from events.models import Event

class EventInline(admin.StackedInline):
    model = Event
    extra = 1
    max_num = 1

class EventTemplate(object):
    """ A page without sidebar """
    verbose_name = _('Event page')
    template_name = "events/pages/event.html"
    sections = [MainSection,]
    options_form = PageOptionsForm
    inlines = [EventInline,]            
    
template_registry.register_template(EventTemplate)

from morumbi.cms.models import BaseModule
from morumbi.cms.forms import ModuleForm

class EventDetailModule(BaseModule):
    """
    Insert event information on page.
    """
    template = "events/modules/event_detail.html"
    verbose_name = _(u"Event detail")
    
    class Meta:
        proxy = True
        
module_registry.register("eventdetailmodule", EventDetailModule)

class EventListModule(BaseModule):
    """
    List 10 newest events
    """
    template = "events/modules/event_list.html"
    verbose_name = _(u"Event list")
    
    class Meta:
        proxy = True

    def get_extra_context(self):
        c = {}
        c.update(self.extra_context or {})
        c.update({'object_list': Event.objects.all()[:10]})
        return c
        
    def process_request(self, request, **kwargs):
        event_list = Event.objects.all()[:10]
        return self.render(request, self.template, 
                            {'event_list': event_list, 'module': self}, **kwargs)

    def preview(self, context_dict={}):
        event_list = Event.objects.all()[:10]
        context_dict.update({'event_list': event_list})
        return super(EventListModule, self).preview(context_dict, 
                                template_name=self.template)
                                
module_registry.register("eventlistmodule", EventListModule)
        