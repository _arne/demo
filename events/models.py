from django.db import models
from django.utils.translation import ugettext_lazy as _

class Event(models.Model):
    page = models.ForeignKey('cms.Page', verbose_name=_("page"))
    title = models.CharField(max_length=255)
    date = models.DateField()
    description = models.TextField(blank=True, null=True)
    
    def __unicode__(self):
        return u"%s (%s)"%(self.title, self.date)
        
    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")
        ordering = ('-date',)
